package weatherapi

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"net/url"
	"os"
)

const (
	WEATHERSTACK_DOMAIN   = "api.weatherapi.com"
	CURRENT_API_JSON_PATH = "/v1/current.json"
	API_KEY_ENV_VAR       = "WEATHER_API_KEY"
)

func GetCurrentWeather(location string) (*CurrentResponse, error) {
	apiKey := fetchApiKey()
	url := getCurrentWeatherUrl(apiKey, location)

	if os.Getenv("DEBUG") == "true" {
		log.Printf("Fetching weather data from %s\n", url)
	}

	resp, err := http.Get(url)
	if err != nil {
		log.Fatalf("Cannot establish connection to WeatherAPI: %v\n", err)
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		log.Printf("Received non-200 response: %d\n", resp.StatusCode)
		errorMessage, err := decodeErrorResponse(resp)
		if err != nil {
			return nil, err
		}

		return nil, errors.New(errorMessage)
	}

	currentResponse, err := decodeWeatherResponse(resp)
	if err != nil {
		return nil, err
	}

	if os.Getenv("DEBUG") == "true" {
		log.Printf("Received weather data for %s: %v\n", location, currentResponse)
	}

	return currentResponse, nil
}

func fetchApiKey() string {
	return os.Getenv(API_KEY_ENV_VAR)
}

func getCurrentWeatherUrl(apiKey string, location string) string {
	queryParams := url.Values{
		"key": {apiKey},
		"q":   {location},
		"aqi": {"no"},
	}

	url := url.URL{
		Scheme:   "https",
		Host:     WEATHERSTACK_DOMAIN,
		Path:     CURRENT_API_JSON_PATH,
		RawQuery: queryParams.Encode(),
	}

	return url.String()
}

func decodeErrorResponse(response *http.Response) (string, error) {
	var errorResponse ErrorResponse
	err := json.NewDecoder(response.Body).Decode(&errorResponse)
	if err != nil {
		log.Fatalf("Error decoding error response: %v\n", err)
		return "", err
	}

	code, message := errorResponse.Error.Code, errorResponse.Error.Message
	log.Printf("Received error message from WeatherAPI (code %d): %s\n", code, message)
	return errorResponse.Error.Message, nil
}

func decodeWeatherResponse(response *http.Response) (*CurrentResponse, error) {
	var currentResponse CurrentResponse
	err := json.NewDecoder(response.Body).Decode(&currentResponse)
	if err != nil {
		log.Fatalf("Error decoding weather response: %v\n", err)
		return nil, err
	}

	return &currentResponse, nil
}
