package server

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	api "gitlab.com/mattrym/weatherapp/pkg/weatherapi"
)

type WeatherResponse struct {
	City      string  `json:"city"`
	Country   string  `json:"country"`
	Condition string  `json:"condition"`
	TempF     float64 `json:"temp_f"`
	Icon      string  `json:"icon"`
}

func WeatherHandler(w http.ResponseWriter, r *http.Request) {
	pathParams := mux.Vars(r)
	city := pathParams["city"]

	currentWeather, err := api.GetCurrentWeather(city)
	if err != nil {
		log.Printf("Error fetching weather data: %v\n", err)
		respondWith404(w)
		return
	}

	respondWith200(w, WeatherResponse{
		City:      currentWeather.Location.Name,
		Country:   currentWeather.Location.Country,
		Condition: currentWeather.Current.Condition.Text,
		TempF:     currentWeather.Current.TempF,
		Icon:      currentWeather.Current.Condition.Icon,
	})
}

func respondWith200(w http.ResponseWriter, response interface{}) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func respondWith404(w http.ResponseWriter) {
	w.WriteHeader(http.StatusNotFound)
	w.Header().Set("Content-Type", "application/json")

	json.NewEncoder(w).
		Encode(map[string]string{
			"error": "Could not fetch weather data",
		})
}
