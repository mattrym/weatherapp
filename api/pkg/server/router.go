package server

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

func Run(portNo int) {
	server := buildHttpServer(portNo)

	go func() {
		log.Printf("Starting server on port %d\n", portNo)
		log.Fatal(server.ListenAndServe())
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutting down server...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := server.Shutdown(ctx); err != nil {
		log.Fatalf("Could not gracefully shutdown the server: %v\n", err)
	}
}

func buildHttpServer(portNo int) *http.Server {
	router := mux.NewRouter()

	router.Handle("/location/{city}/weather", handleWithMiddleware(http.HandlerFunc(WeatherHandler)))

	return &http.Server{
		Addr:         fmt.Sprintf(":%d", portNo),
		Handler:      router,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
}

func handleWithMiddleware(handler http.Handler) http.Handler {
	loggingHandler := handlers.LoggingHandler(os.Stdout, handler)
	compressHandler := handlers.CompressHandler(loggingHandler)
	corsHandler := handlers.CORS(
		handlers.AllowedHeaders([]string{"X-Requested-With"}),
		handlers.AllowedOrigins([]string{"http://localhost:8000"}),
		handlers.AllowedMethods([]string{"GET"}),
	)(compressHandler)

	return corsHandler
}
