package main

import (
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/mattrym/weatherapp/pkg/server"
)

type Config struct {
	ServerPort int    `json:"server_port,omitempty,default=8080"`
	APIKey     string `json:"api_key"`
	Debug      bool   `json:"debug,omitempty,default=false"`
}

func main() {
	config, err := loadConfig()
	if err != nil {
		fmt.Println("Error loading config:", err)
		os.Exit(1)
	}

	if config.APIKey == "" {
		fmt.Println("Error: API key is required")
		os.Exit(1)
	}

	os.Setenv("WEATHER_API_KEY", config.APIKey)
	os.Setenv("DEBUG", fmt.Sprintf("%t", config.Debug))
	server.Run(config.ServerPort)
}

func loadConfig() (*Config, error) {
	var config Config
	file, err := os.Open("config.json")
	if err != nil {
		return nil, err
	}

	defer file.Close()

	decoder := json.NewDecoder(file)
	err = decoder.Decode(&config)
	if err != nil {
		return nil, err
	}

	return &config, nil
}
