module Main exposing (Model, Msg, init, main, subscriptions, update, view)

import Browser
import Debounce exposing (Debounce)
import Html as H exposing (Html)
import Html.Attributes as A
import Html.Events as E
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as DecodeP
import Task
import Url.Builder exposing (crossOrigin)


main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }


type alias Flags =
    { baseUrl : String
    }


type alias Model =
    { baseUrl : String
    , rawInput : String
    , input : String
    , weather : Weather
    , debounce : Debounce String
    }


type Weather
    = Empty
    | Loaded WeatherData
    | Failed Http.Error


type alias WeatherData =
    { city : String
    , country : String
    , temperature : Float
    , icon : String
    , condition : String
    }


init : Flags -> ( Model, Cmd Msg )
init flags =
    ( Model
        flags.baseUrl
        ""
        ""
        Empty
        Debounce.init
    , Cmd.none
    )


type Msg
    = InputReceived String
    | InputDecounced String
    | DebounceMsg Debounce.Msg
    | GotWeather (Result Http.Error WeatherData)


debounceConfig : Debounce.Config Msg
debounceConfig =
    { strategy = Debounce.later 750
    , transform = DebounceMsg
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        InputReceived input ->
            let
                ( debounce, cmd ) =
                    Debounce.push debounceConfig input model.debounce
            in
            ( { model | rawInput = input, debounce = debounce }, cmd )

        InputDecounced input ->
            ( { model | input = input }, fetchWeather model.baseUrl input )

        DebounceMsg debMsg ->
            let
                ( debounce, cmd ) =
                    Debounce.update
                        debounceConfig
                        (Debounce.takeLast emitDebouncedValue)
                        debMsg
                        model.debounce
            in
            ( { model | debounce = debounce }, cmd )

        GotWeather (Ok weatherData) ->
            ( { model | weather = Loaded weatherData }, Cmd.none )

        GotWeather (Err err) ->
            ( { model | weather = Failed err }, Cmd.none )


emitDebouncedValue : String -> Cmd Msg
emitDebouncedValue input =
    Task.perform InputDecounced (Task.succeed input)


fetchWeather : String -> String -> Cmd Msg
fetchWeather baseUrl city =
    Http.get
        { url = buildWeatherUrl baseUrl city
        , expect = Http.expectJson GotWeather weatherDecoder
        }


buildWeatherUrl : String -> String -> String
buildWeatherUrl baseUrl city =
    crossOrigin baseUrl [ "location", city, "weather" ] []


weatherDecoder : Decoder WeatherData
weatherDecoder =
    Decode.succeed WeatherData
        |> DecodeP.required "city" Decode.string
        |> DecodeP.required "country" Decode.string
        |> DecodeP.required "temp_f" Decode.float
        |> DecodeP.required "icon" Decode.string
        |> DecodeP.required "condition" Decode.string


view : Model -> Html Msg
view model =
    H.div [ A.class "wrap" ]
        [ viewSearchBar model.rawInput
        , viewWeather model.weather model.input
        ]


viewWeather : Weather -> String -> Html Msg
viewWeather weather input =
    case weather of
        Empty ->
            H.div [] []

        Loaded weatherData ->
            viewWeatherData weatherData

        Failed _ ->
            viewMissingData input


viewWeatherData : WeatherData -> Html msg
viewWeatherData weather =
    H.div [ A.class "weatherWidget" ]
        [ H.div [ A.class "weatherLocation" ]
            [ H.span [ A.class "city" ] [ H.text weather.city ]
            , H.span [ A.class "country" ] [ H.text weather.country ]
            ]
        , H.div [ A.class "weatherDetails" ]
            [ H.span [ A.class "temp" ] [ H.text <| String.fromFloat weather.temperature ++ "°F" ]
            , H.div [ A.class "cond" ]
                [ H.img [ A.class "icon", A.src ("https:" ++ weather.icon) ] []
                , H.span [ A.class "description" ] [ H.text weather.condition ]
                ]
            ]
        ]


viewMissingData : String -> Html msg
viewMissingData input =
    H.div [ A.class "error" ]
        [ H.span [ A.class "errorMessage" ]
            [ H.text <| "Could not find data for location: " ++ input ]
        , H.span [ A.class "errorSuggestion" ]
            [ H.text "Please try again with a different location." ]
        ]


viewSearchBar : String -> Html Msg
viewSearchBar rawInput =
    H.div [ A.class "search" ]
        [ H.input
            [ A.class "searchTerm"
            , A.placeholder "Please enter location..."
            , A.type_ "text"
            , A.value rawInput
            , E.onInput InputReceived
            ]
            []
        ]


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none
