# weatherapp

Run `docker compose -f docker-compose.yml up` to scale up containers and start app.
Run `docker compose -f docker-compose.yml down` to scale it down.

App uses [WeatherAPI.com](https://www.weatherapi.com/) to get current weather data. In order to make it work, you need to sign up there and fetch API key, and later put it in the `api/config.json` file. API is out of charge.

